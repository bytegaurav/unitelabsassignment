package com.unitelabs.models;
/**
 * 
 */

/**
 * @author gaurav
 * Represent a 2D axis aligned rectangle
 *
 */
public class Rectangle {
	
	public int x;	//top left coordinate 
	public int y;	//top left coordinate
	public int w;	//width 
	public int h;	//height
	public int x2;	//bottom right coordinate
	public int y2;	//bottom right coordinate
	
	public String label;

	public Rectangle() {
		
	}
	
	public Rectangle(int x,int y,int w,int h) {
		this.x=x;
		this.y=y;
		this.w=w;
		this.h=h;
	}
	
}

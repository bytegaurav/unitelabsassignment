package com.unitelabs.models;
/**
 * 
 */

/**
 * @author gaurav
 *
 */

public class Event {

	public EventTypeEnum type;
	public Rectangle rect;

	public Event(EventTypeEnum type, Rectangle rect) {
		this.type = type;
		this.rect = rect;
	}

}

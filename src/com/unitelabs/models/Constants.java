/**
 * 
 */
package com.unitelabs.models;

/**
 * @author gaurav
 *
 */
public class Constants {
	public static final String BLANK_STRING = "";
	public static final String SPACE_STRING = " ";
	public static final String SEPARATER_COMMA_STRING = ",";
	public static final String SPERATOR_AND_STRING = "and";
	public static final String SPERATOR_DASH_STRING = "-";
	public static final String RECT_PREFIX_STRING = "R";

}

/**
 * 
 */
package com.unitelabs.util;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.unitelabs.intersectingrectangles.Plane;
import com.unitelabs.models.Constants;
import com.unitelabs.models.InputMaps;
import com.unitelabs.models.Rectangle;

/**
 * @author gaurav
 *
 */
public class InputManager {
	
	
	public static Plane readInput(String filename) throws Exception {
		
		try {
			Gson g = new Gson();
			JsonReader reader=null;
			reader = new JsonReader(new FileReader(filename));
			Plane plane = g.fromJson(reader, Plane.class);
			Validate.validate(plane);
			if (plane.rects.length > 10) {
				plane.rects = Arrays.copyOfRange(plane.rects, 0, 10);
			}
			return plane;
		}catch(FileNotFoundException e) {
			System.out.println("Invalid file path");
			return null;
		}catch(EOFException e) {
			return null;
		}
		
	}
	
	public static Plane processInput(Plane plane) {
		
					// calculating lower right coords and populating hashes
					for (int i = 0; i < plane.rects.length; i++) {
						plane.rects[i].x2 = plane.rects[i].x + plane.rects[i].w;
						plane.rects[i].y2 = plane.rects[i].y + plane.rects[i].h;
						plane.rects[i].label = Constants.RECT_PREFIX_STRING + (i + 1);
					}
		
		return plane;
	}
	/*
	 *  mapping for x start and end coords with respecitve rectangles, to speed up
	 *  the lookup 
	 */
	
	public static InputMaps prepareInputMaps(Plane plane){
		InputMaps inputmaps = new InputMaps();
		for (Rectangle  r : plane.rects) {
			inputmaps.rectMapEntryX.put(r.x, r);
			inputmaps.rectMapExitX.put(r.x2, r);
		}
		return inputmaps;
	}
	
}

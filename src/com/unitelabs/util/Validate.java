package com.unitelabs.util;

import com.unitelabs.intersectingrectangles.Plane;

public class Validate {

	public static void validate(Plane plane) throws Exception {
		if(plane==null) {
			throw new Exception("Empty json found");
		}
		
		if(plane.rects==null) {
			throw new Exception("Json keys missing");
		}
	}
}

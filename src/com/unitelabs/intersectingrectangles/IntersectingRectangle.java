package com.unitelabs.intersectingrectangles;
import com.unitelabs.models.InputMaps;
import com.unitelabs.util.InputManager;


public class IntersectingRectangle {

	public static void main(String[] args) throws Exception {
		
			Plane plane = InputManager.readInput(args[0]);
			//print input
			plane.printPlane();
			plane = InputManager.processInput(plane);
			
			// mapping for x start and end coords with respective rectangles, to speed up the lookup
			InputMaps inputMaps = InputManager.prepareInputMaps(plane);
		
			// get the bounds for search space
			Bounds bounds = plane.getBounds();
			
			// sweeps a line a process plane
			Sweep linesweep = new Sweep();
			linesweep.sweeper(inputMaps.rectMapEntryX, inputMaps.rectMapExitX, bounds);
			//print output
			linesweep.printIntersections();
	}
}

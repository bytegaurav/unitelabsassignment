package com.unitelabs.intersectingrectangles;

/**
 * Class Sweep contains data members and functions to line sweep the plane and find intersecting
 * rectangles. 
 */

import java.util.*;

import com.unitelabs.models.Constants;
import com.unitelabs.models.Event;
import com.unitelabs.models.EventTypeEnum;
import com.unitelabs.models.Rectangle;

/**
 * @author gaurav
 *	C
 */
public class Sweep {

	
	private List<Event> eventsList;
	private Map<String, Rectangle> intersectionsMap;
	private Map<String, Rectangle> currentMap;

	public Sweep() {
		this.eventsList = new ArrayList<Event>();
		this.intersectionsMap = new HashMap<String, Rectangle>();
		this.currentMap = new HashMap<String, Rectangle>();

	}

	public Map<String, Rectangle> getIntersections() {
		return this.intersectionsMap;
	}

	/*
	 * Sweeper sweeps from left to right and mark entry and exit events
	 */
	public void sweeper(Map<Integer, Rectangle> entryMap, Map<Integer, Rectangle> exitMap, Bounds bounds) {

		for (int i = bounds.left - 1; i <= bounds.right + 1; i++) {

			if (entryMap.get(i) != null) {
				this.eventsList.add(new Event(EventTypeEnum.ENTRY, entryMap.get(i)));
			}
			if (exitMap.get(i) != null) {
				this.eventsList.add(new Event(EventTypeEnum.EXIT, exitMap.get(i)));
			}
		}
		classifier();
		generateNonBinaryIntersections();

	}

	/*
	 * Based on events marked by sweeper, classifiers updates currently intersecting
	 * rectangles or if event is of type exit, it removes rectangles from current.
	 */

	private void classifier() {

		eventsList.forEach(e->{
			
			if (e.type.equals(EventTypeEnum.ENTRY)) {
				if (!this.currentMap.isEmpty()) {
					sweepHorizontally(e.rect);
				}
				this.currentMap.put(e.rect.label, e.rect);

			}

			if (e.type.equals(EventTypeEnum.EXIT)) {
				this.currentMap.remove(e.rect.label);
				updateWidth(e.rect);

			}

		});
		
	}

	/*
	 * sweepHorizontally sweeps rectangles in current array. If they intersect on y
	 * axis too, it is placed in intersection map.
	 * 
	 */
	private void sweepHorizontally(Rectangle rect) {

		for (Map.Entry<String, Rectangle> c : this.currentMap.entrySet()) {

			if (Integer.max(c.getValue().y, rect.y) < c.getValue().y2) {

				if (this.intersectionsMap.get(rect.label + Constants.SPERATOR_DASH_STRING + c.getKey()) == null) {

					Rectangle intersected = new Rectangle();
					intersected.x = rect.x;
					intersected.y = Integer.max(c.getValue().y, rect.y);
					intersected.h = Integer.min(c.getValue().y2, rect.y2) - Integer.max(c.getValue().y, rect.y);
					this.intersectionsMap.put(c.getKey() + Constants.SPERATOR_DASH_STRING + rect.label, intersected);

				}

			}

		}

	}

	/*
	 * updatewidth is called on exit event to mark width of a rectangle. this is
	 * done for only those rectangles which are already marked as intersecting
	 * 
	 */

	private void updateWidth(Rectangle rect) {

		for (Map.Entry<String, Rectangle> i : this.intersectionsMap.entrySet()) {

			Rectangle copy = i.getValue();

			if (i.getKey().contains(rect.label)) {

				if (copy.w == 0) {

					copy.w = rect.x2 - copy.x;

					this.intersectionsMap.put(i.getKey(), copy);

				}

			}

		}

	}

	/*	
	 *  findWithSubKey finds rect object in intersectionMap, if part of key matches the map key.
	 */
	
	private HashMap<String, Rectangle> findWithSubKey(String key) {
		HashMap<String, Rectangle> map = new HashMap<String, Rectangle>();
		for (Map.Entry<String, Rectangle> r : this.intersectionsMap.entrySet()) {
			if (r.getKey().contains(key) && r.getKey().indexOf(key) < 2) {
				map.put(r.getKey(), r.getValue());
			}
		}
		return map;

	}
	/*
	 * generateNonBinaryIntersections generates all possible combination where more than 2 rectangles are 
	 * intersecting. 
	 */
	
	private void generateNonBinaryIntersections() {

		HashMap<String, Boolean> visited = new HashMap<>();
		HashMap<String, Rectangle> temp = new HashMap<>();

		for (Map.Entry<String, Rectangle> r : this.intersectionsMap.entrySet()) {
			String key = r.getKey().split(Constants.SPERATOR_DASH_STRING)[0];

			if (visited.get(key) != null) {
				continue;
			} else {
				visited.put(key, true);
			}

			String unionkey = Constants.BLANK_STRING;
			HashMap<String, Rectangle> unionMap = findWithSubKey(key);

			Rectangle tempRect = new Rectangle();

			tempRect.x = Integer.MIN_VALUE;
			tempRect.w = Integer.MAX_VALUE;
			tempRect.y = Integer.MIN_VALUE;
			tempRect.h = Integer.MAX_VALUE;

			for (Map.Entry<String, Rectangle> u : unionMap.entrySet()) {
				unionkey += u.getKey().replace(key + Constants.SPERATOR_DASH_STRING, Constants.BLANK_STRING) + Constants.SPERATOR_DASH_STRING;
				tempRect.x = Integer.max(tempRect.x, u.getValue().x);
				tempRect.w = Integer.min(tempRect.w, u.getValue().w);
				tempRect.y = Integer.max(tempRect.y, u.getValue().y);
				tempRect.h = Integer.min(tempRect.h, u.getValue().h);

			}
			unionkey = key + "-" + unionkey;
			unionkey = unionkey.substring(0, unionkey.length() - 1);

			temp.put(unionkey, tempRect);

		}
		
		this.intersectionsMap.putAll(temp);
		

	}

	
	
	public void printIntersections() {

		System.out.println("Intersections:");
		int counter = 1;
		
		Map<String, Rectangle> sortedMap = new TreeMap<String, Rectangle>(new Comparator<String>() {
			@Override
			public int compare(String i1, String i2) {
				return i1.compareTo(i2);
			}
		});
		sortedMap.putAll(this.intersectionsMap);
		
		
		for (Map.Entry<String, Rectangle> i : sortedMap.entrySet()) {

			String[] rectLabels = i.getKey().split(Constants.SPERATOR_DASH_STRING);
			
			StringBuilder rectnames = new StringBuilder(Constants.BLANK_STRING);
			for (int j = 0; j < rectLabels.length; j++) {
				
				rectnames.append(rectLabels[j].replace(Constants.RECT_PREFIX_STRING, Constants.BLANK_STRING));

				if (j == rectLabels.length - 2) {
					rectnames.append(Constants.SPACE_STRING);
					rectnames.append(Constants.SPERATOR_AND_STRING);
					rectnames.append(Constants.SPACE_STRING);
					
				}
				if (j < rectLabels.length - 2) {
					rectnames.append(Constants.SEPARATER_COMMA_STRING);
					rectnames.append(Constants.SPACE_STRING);
					

				}

			}

			System.out.println("\t" + counter + ": Between rectangles " + rectnames + " at " + "(" + i.getValue().x
					+ "," + i.getValue().y + ")" + ", w=" + i.getValue().w + ", h=" + i.getValue().h + ".");

			counter++;
		}

	}

}

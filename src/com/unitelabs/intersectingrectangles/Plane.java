package com.unitelabs.intersectingrectangles;
/**
 * Class Plane represents a cartesian plane, which will have rectangles.
 * Also, it defines helper methods which are related to plane
 * 
 */

import com.unitelabs.models.Rectangle;

/**
 * @author gaurav
 * 
 */
public class Plane {
	
	public Rectangle[] rects;
	
	
	public Bounds getBounds() {
		Bounds b = new Bounds(Integer.MAX_VALUE,
								Integer.MIN_VALUE, 
								Integer.MAX_VALUE,
								Integer.MIN_VALUE);
		for (int i = 0; i < rects.length; i++) {
			b.left = Integer.min(rects[i].x, b.left);
			b.right = Integer.max(rects[i].x2, b.right);
			b.top = Integer.min(rects[i].y, b.top);
			b.bottom = Integer.max(rects[i].y2, b.bottom);   
		 }
		return b;
	}
	
	public void printPlane() {
		System.out.println("Input:");
		for(int i=0; i<this.rects.length; i++) {
			System.out.println("\t"+(i+1)+": Rectangle at ("+this.rects[i].x+","+this.rects[i].x+"),"
					+ " w="+this.rects[i].w+", h="+this.rects[i].h+".");
		}
	}
	

}
/*Class Bounds represent working part of plane where all objects are placed.
 * This is required to limit the search space and reduce computation
 * 	
 * */
 class Bounds{
	
	public int left;
	public int right;
	public int top;
	public int bottom;
	
	public Bounds(int left, int right, int top, int bottom) {
		this.left=left;
		this.right=right;
		this.bottom=bottom;
		this.top=top;
		
	}
}

package com.unitelabs.intersectingrectangles;



import java.util.*;

import org.junit.*;

import com.unitelabs.models.InputMaps;
import com.unitelabs.models.Rectangle;
import com.unitelabs.util.InputManager;




public class SweepTest {
	
	
	
	@Test
	public void test_intersecting_rect() {
		try {
			Plane plane = InputManager.readInput("resources/input.json");
			plane = InputManager.processInput(plane);
			InputMaps inputMaps = InputManager.prepareInputMaps(plane);
			Sweep lineSweep= new Sweep();
			lineSweep.sweeper(inputMaps.rectMapEntryX, inputMaps.rectMapExitX, plane.getBounds());
			Assert.assertNotNull(lineSweep.getIntersections());
			
			
		}catch(Exception e) {
			Assert.fail(e.getMessage());
		}
		
	}
	
	

	
	@Test
	public void test_nonintersecting_rect() {
		try {
			Map<String, Rectangle> expected= new HashMap<>();
			Plane plane = InputManager.readInput("resources/nonIntersectingInput.json");
			plane = InputManager.processInput(plane);
			InputMaps inputMaps = InputManager.prepareInputMaps(plane);
			Sweep lineSweep= new Sweep();
			lineSweep.sweeper(inputMaps.rectMapEntryX, inputMaps.rectMapExitX, plane.getBounds());
			Assert.assertEquals(expected, lineSweep.getIntersections());
			
		}catch(Exception e) {
			Assert.fail(e.getMessage());
		}
		
	}

	@Test
	public void test_concentric_rect() {
		try {
			int expectedIntersectionCount=8;
			
			
			
			Plane plane = InputManager.readInput("resources/concentricInput.json");
			plane = InputManager.processInput(plane);
			InputMaps inputMaps = InputManager.prepareInputMaps(plane);
			Sweep lineSweep= new Sweep();
			lineSweep.sweeper(inputMaps.rectMapEntryX, inputMaps.rectMapExitX, plane.getBounds());
			
			Assert.assertEquals(expectedIntersectionCount, lineSweep.getIntersections().keySet().size());
			
		}catch(Exception e) {
			Assert.fail(e.getMessage());
		}
		
	}
	
	

}

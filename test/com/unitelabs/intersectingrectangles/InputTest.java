package com.unitelabs.intersectingrectangles;


import org.junit.*;
import org.junit.rules.ExpectedException;


import com.unitelabs.util.InputManager;


public class InputTest {
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();
	 

	@Test
	public void test_valid_input() {
		
		try {
		Plane plane = InputManager.readInput("resources/input.json");
		Assert.assertNotNull(plane);
		
		}catch(Exception e) {
			Assert.fail("Failed with valid input");
		}
		
		
	}
	@Test
	public void test_invalid_input() {
		try {
			InputManager.readInput("resources/invalid.json");
			Assert.fail("failed to check invalid input");
		}catch(Exception e) {}
	}
	
	@Test
	public void test_empty_input() {
		try {
			InputManager.readInput("resources/empty.json");
			Assert.fail("failed to check empty input");
		}catch(Exception e) {}
		
	}


}
